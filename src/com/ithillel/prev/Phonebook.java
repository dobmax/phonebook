package com.ithillel.prev;

public class Phonebook {
    private static final Record[] EMPTY_RECORDS = new Record[0];

    private Record[] records = EMPTY_RECORDS;

    public void add(Record newRecord) {
        records = add(newRecord, records);
    }

    public void add(String name, String number) {
        add(new Record(name, number));
    }

    private Record[] add(Record newRecord, Record[] target) {
        Record[] newRecords = new Record[target.length + 1];

        for (int i = 0; i < target.length; i++) {
            newRecords[i] = target[i];
        }

        newRecords[newRecords.length - 1] = newRecord;
        return newRecords;
    }

    public Record get(String name) {
        for (int i = 0; i < records.length; i++) {
            if (records[i].getName().equals(name)) {
                return records[i];
            }
        }
        return null;
    }

    public Record[] getAll(String name) {
        Record[] foundRecords = EMPTY_RECORDS;

        for (int i = 0; i < records.length; i++) {
            if (records[i].getName().equals(name)) {
                foundRecords = add(records[i], foundRecords);
            }
        }

        return foundRecords;
    }


}
